# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "9"

practice = "day_"+day+"/practice.txt" 
extra_test = "day_"+day+"/extra_test.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")

class PascalTriangle:
    def __init__(self,top_line):
        self.triangle = np.zeros(shape=(1,len(top_line)))
        self.triangle[0,:] = top_line
    
    def construct_triangle(self):
        all_zeros = False
        line = 0
        self.triangle_top_width = self.triangle.shape[1]

        while all_zeros == False:

            diff_line = np.diff(self.triangle[line,0:(self.triangle_top_width-line)])
            diff_line = np.pad(diff_line,(0,line+1),'constant',constant_values=(0,0))

            # Initially, I created a n,n array of zeros at the start (n being the length of the line)
            # However, with negative numbers, it can be that the triangle broadens again at a certain point
            # Therefore, I had to stack the lines to the triangle
            self.triangle = np.vstack((self.triangle,diff_line))

            # Previously, I used the sum of the diff line.
            # However, this creates problems when de line contains e.g. 1 and -1
            # Therefore, changed it to all zeros
            all_zeros = not diff_line.any()
            line += 1        

        self.triangle = self.triangle[0:line+1,:]

    def expand_triangle(self):
        # Add an extra column to the triangle
        self.triangle = np.hstack((self.triangle,np.zeros((self.triangle.shape[0],1))))

        h_triangle = np.shape(self.triangle)[0]

        for i in reversed(range(0,h_triangle-1)):
            next_value = self.triangle[i,(self.triangle_top_width-i-1)] + self.triangle[i+1,(self.triangle_top_width-i-1)] 
            self.triangle[i,(self.triangle_top_width-i)] = next_value

        return next_value
        

triangles = []
for line in input:
    input_numbers = [int(x) for x in line.split()]
    triangles.append(PascalTriangle(input_numbers))

histories_next_values = []
for triangle in triangles:
    triangle.construct_triangle()    
    next_value = triangle.expand_triangle()
    histories_next_values.append(next_value)

print("The answer to part A is: ",sum(histories_next_values))

# 2043183907 is too high
# 
# 52265672 is too low
#