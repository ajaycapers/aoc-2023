# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "4"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np
from math import floor

# Make a separate class for the cards
class Game:
    def __init__(self,card_no,winning_numbers,numbers_you_have):
        self.card_no = card_no
        self.winning_numbers = winning_numbers
        self.numbers_you_have = numbers_you_have
        self.number_of_copies = 1

    def copy_card(self,times_to_copy):
        self.number_of_copies += times_to_copy

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")

carddeck = []
for line in input:
    card_no = int(line.split(':')[0].split()[1])
    winning_numbers = set([int(x) for x in line.split(':')[1].split('|')[0].split()])
    numbers_you_have = set([int(x) for x in line.split(':')[1].split('|')[1].split()])
    carddeck.append(Game(card_no, winning_numbers, numbers_you_have))

scratchcard_worth = []
for card in carddeck:
    overlap = card.winning_numbers.intersection(card.numbers_you_have)
    card_value = floor(2**(len(overlap)-1))
    scratchcard_worth.append(card_value)

print("The answer to part A is: ", sum(scratchcard_worth))

### Part B ###

total_number_of_cards = 0
# You can forward loop through the carddeck as copies can only be created of later cards
for i,card in enumerate(carddeck):
    overlap = card.winning_numbers.intersection(card.numbers_you_have)
    no_of_copies = len(overlap)
    if no_of_copies > 0:
        for copy_this_card in carddeck[i+1:i+no_of_copies+1]:
            copy_this_card.copy_card(card.number_of_copies)
    total_number_of_cards += card.number_of_copies

print("The answer to part B is: ",total_number_of_cards)
