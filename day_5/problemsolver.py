# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "5"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np

class Source_to_destination():
    def __init__(self):
        self.source_category = []
        self.destination_category = []
        self.ranges = []

    def add_range(self,range):
        self.ranges.append(range)

    def add_source_and_destination(self,source,destination):
        self.source_category.append(source)
        self.destination_category.append(destination)

    def transfer(self,seed):
        coordinate = seed
        export_string = []
        export_string.append(''.join(['seed-']))
        for ranges,destination_category in zip(self.ranges,self.destination_category):
            export_string.append(''.join([str(coordinate),'-',destination_category,'-']))

            for range in ranges:
                if range[1] <= coordinate <= (range[1] + range[2] - 1):
                    destination = coordinate - (range[1] - range[0])
                    coordinate = destination
                    break
        
        export_string.append(''.join([str(coordinate)]))
        
        # print(''.join(export_string))
        return coordinate
    
    def transfer_range(self,seed,seed_range):
        # First, create a dictionary of the matching values
        next_coordinate = dict()
        for ranges,destination_category in zip(self.ranges,self.destination_category): # This gets the list of ranges
            for range_list in ranges:
                for coordinate in range(range_list[1],range_list[1]+range_list[2]-1):
                    next_coordinate[coordinate] = coordinate - (range_list[1] - range_list[0])

        complete_seed_range = list(range(seed,seed+seed_range))
        locations = [next_coordinate[seed] if seed in next_coordinate else seed for seed in complete_seed_range ]
      
        return min(locations)
    
    def transfer_all_seeds(self,all_seeds):
        # So, we start with a list containing all seeds
        new_coordinates = set(all_seeds)
        for ranges,destination_category in zip(self.ranges,self.destination_category): # This gets the list of ranges
            print(destination_category)
            all_seeds = new_coordinates.copy()
            new_coordinates.clear()

            for range_list in ranges:
                affected_seeds = set(range(range_list[1],range_list[1]+range_list[2]))
                affected_seeds.intersection_update(all_seeds)
                all_seeds.difference_update(affected_seeds)

                difference = range_list[1] - range_list[0]

                new_coordinates.update([(x-difference) for x in affected_seeds])

            new_coordinates.update(all_seeds)

        return new_coordinates 

    def transfer_inverse(self,location):
        coordinate = location

        for ranges,source_category in zip(reversed(self.ranges),reversed(self.source_category)):
            print(source_category)
            for range_list in ranges:
                if range_list[0] <= coordinate <= (range_list[0] + range_list[2] - 1):
                    source = coordinate + (range_list[1] - range_list[0])
                    coordinate = source
                    break
                
        return coordinate

    
# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")
    seeds = [int(x) for x in input.pop(0).split(':')[1].split()]
    input.remove('')

    maps = []
    a_to_b = []

    a_to_b = Source_to_destination()

    ranges = []
    for line in input:

        # Give the map names
        if '-' in line:
           source_to_destination = line.split('-')
           source_category = source_to_destination[0]
           destination_category = source_to_destination[2].split()[0]
           a_to_b.add_source_and_destination(source_category,destination_category)
           if ranges:
               a_to_b.add_range(ranges)
               ranges = []

        # If the line is empty, continue
        elif not line:
            continue

        # Otherwise, find the ranges
        else:
            ranges.append([int(x) for x in line.split()])

    a_to_b.add_range(ranges)

    print(a_to_b.source_category)

locations = []

for seed in seeds:
    location = a_to_b.transfer(seed)
    locations.append(location)

print("The answer to part A is: ",min(locations))

# For part B, consider the seed numbers as ranges
# locations = []
# for seed,seed_range in zip(seeds[::2],seeds[1::2]):
#     for value in range(seed,seed+seed_range):
#         location = a_to_b.transfer(value)
#         locations.append(location)

# Well, that took too long. Try to "shortcutting", guessing that one of the extremes is likely to give the answer:
# locations = []
# for seed,seed_range in zip(seeds[::2],seeds[1::2]):
#     for value in [seed,seed+seed_range]:
#         location = a_to_b.transfer(value)
#         locations.append(location)

# Well, "shortcutting" does not give the right answer.
# locations = []
# all_seeds = []
# for seed,seed_range in zip(seeds[::2],seeds[1::2]):
#     list(map(all_seeds.append, range(seed,seed+seed_range)))
#     all_seeds = set(all_seeds)
#     location = a_to_b.transfer_all_seeds(all_seeds)
#     locations.append(list(location))
#     all_seeds = []
#     print(locations)

# Based on an advise on reddit, try the inverse and find the lowest possible number
location = 1
answer_found = False
while answer_found == False:
    seed_to_check = a_to_b.transfer_inverse(location)
    
    # Check if the location is in the range of the initial seeds
    for seed,seed_range in zip(seeds[::2],seeds[1::2]):
        if seed <= seed_to_check <= (seed+seed_range):
            print("Matching seed! The answer to part B is: ",location)
            answer_found = True
        else:
            pass
            # print(location," is not a correct answer")

    location += 1

print("The answer to part B is: ",location)