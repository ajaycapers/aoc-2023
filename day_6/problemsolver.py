# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "6"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np
from math import prod

# Open the dataset
with open(dataset) as f:
    input = f.read().split('\n')
    times = [int(x) for x in input[0].split(':')[1].split()]
    distances = [int(x) for x in input[1].split(':')[1].split()]

# Loop through the different races
number_of_ways = []
for max_time,target_distance in list(zip(times,distances)):

    distances_achieved = []
    distance = 0
    for press_time in range(0,max_time):
        previous_distance = distance
        travel_time = max_time - press_time
        speed = press_time # m/s
        distance = speed * travel_time

        if distance > target_distance:
            distances_achieved.append(distance)

        elif (distance < previous_distance) and (distance < target_distance):
            break # Distances will only become shorter
    
    number_of_ways.append(len(distances_achieved))

print(number_of_ways)
print("The answer of part A is: ",prod(number_of_ways))

# Part B
with open(dataset) as f:
    input = f.read().split('\n')
    times = [int(input[0].split(':')[1].replace(' ',''))]
    distances = [int(input[1].split(':')[1].replace(' ',''))]

# Loop through the different races
number_of_ways = []
for max_time,target_distance in list(zip(times,distances)):

    distances_achieved = []
    distance = 0
    for press_time in range(0,max_time):
        previous_distance = distance
        travel_time = max_time - press_time
        speed = press_time # m/s
        distance = speed * travel_time

        if distance > target_distance:
            distances_achieved.append(distance)

        elif (distance < previous_distance) and (distance < target_distance):
            break # Distances will only become shorter
    
    number_of_ways.append(len(distances_achieved))

print(number_of_ways)
print("The answer of part B is: ",prod(number_of_ways))



