# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "10"

practice = "day_"+day+"/practice.txt" 
practice2 = "day_"+day+"/practice2.txt" 
practice3 = "day_"+day+"/practice3.txt" 
practice4 = "day_"+day+"/practice4.txt" 
practice5 = "day_"+day+"/practice5.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np

origin_pipe = {'|': ('north','south'), '-': ('east','west'), 'L': ('north','east'), 'J': ('north','west'), 'F': ('south','east'), '7': ('south', 'west')}
pipes = {'|': ('south','north'), '-': ('west','east'), 'L': ('south','west'), 'J': ('south','east'), 'F': ('north','west'), '7': ('north', 'east')}
inside_out = {'|': ('west', 'east'), '-': ('north','south'), 'F': ('west', 'north'), '7': ('north', 'east'), 'J': ('east','south'),'L': ('south', 'west')}

def neighbors(matrix: np.ndarray, coordinate: tuple):
    x,y = coordinate

    x_len, y_len = np.array(matrix.shape) - 1
    nbr = dict()
    nbr_coord = dict()

    origin = matrix[x][y]

    if x > x_len or y > y_len:
        return nbr
    if x != 0:
        nbr['north'] = matrix[x-1][y]
        nbr_coord['north'] = (x-1,y)
    if y != 0:
        nbr['west'] = matrix[x][y-1]
        nbr_coord['west'] = (x,y-1)
    if x != x_len:
        nbr['south'] = matrix[x+1][y]
        nbr_coord['south'] = (x+1,y)
    if y != y_len:
        nbr['east'] = matrix[x][y+1]
        nbr_coord['east'] = (x,y+1)
    return nbr, nbr_coord, origin
    
def identify_connected_pipes(matrix,coordinate):
    nbr, nbr_coord, origin = neighbors(matrix,coordinate)
    connections = []

    for key in nbr.keys():
        pipe = nbr[key]

        # Check if it is a pipe
        if pipe in pipes:
            # If it is a pipe, check whether it connects in the direction that was sought
            if key in pipes[pipe]:
                # If it does, check whether the original pipe connected in that direction
                if origin != 'S':
                    if key in origin_pipe[origin]:
                        connections.append(nbr_coord[key])
                elif origin == 'S':
                    connections.append(nbr_coord[key])
    
    return connections

# Open the dataset
with open(dataset) as f:
    input = f.read().split('\n')
    matrix = np.empty((len(input),len(input[0])),dtype="str")

    for i,line in enumerate(input):
        for j,char in enumerate(line):
            matrix[i,j] = char

# Find the starting position's coordinate
s = np.where(matrix == 'S')
start_coordinate = tuple(zip(*s))[0]

# Find the first two coordinates
connected_pipes = identify_connected_pipes(matrix,start_coordinate)

first_direction = [start_coordinate]
second_direction = [start_coordinate]

first_direction.append(connected_pipes[0])
second_direction.append(connected_pipes[1])

# Find out which coordinate is "inside" the pipe:
connected_pipes.append(start_coordinate)

while first_direction[-1] != second_direction[-1]:

    # First do the first_direction
    connected_pipes = identify_connected_pipes(matrix,first_direction[-1])
    if connected_pipes[0] not in first_direction:
        first_direction.append(connected_pipes[0])
    elif connected_pipes[1] not in first_direction:
        first_direction.append(connected_pipes[1])
    else:
        raise Exception(''.join(["Some case not foreseen at ",str(connected_pipes),". The list of directions is: ",str(first_direction)]))

    # Then do the second direction
    connected_pipes = identify_connected_pipes(matrix,second_direction[-1])

    if connected_pipes[0] not in second_direction:
        second_direction.append(connected_pipes[0])
    elif connected_pipes[1] not in second_direction:
        second_direction.append(connected_pipes[1])
    else:
        raise Exception(''.join(["Some case not foreseen at ",str(connected_pipes),". The list of directions is: ",str(second_direction)]))

print("The answer to part 1 is: ", len(first_direction)-1)

# Part 2

pipe_matrix_LHS = np.zeros_like(matrix, dtype="int")
pipe_matrix_RHS = np.zeros_like(matrix, dtype="int")

previous_coordinate = start_coordinate
for coordinate in first_direction[1:]:
    if matrix[coordinate] in inside_out.keys(): # It is a '-' or a '|'
        in_out = inside_out[matrix[coordinate]]
        
        # Find the previous coordinate to see where you are coming from
        x_diff = coordinate[0] - previous_coordinate[0]
        y_diff = coordinate[1] - previous_coordinate[1]

        # If x_diff > 0, then coming from north. If x_diff < 1, then coming from south.
        # If y_diff > 0, then coming from west. If y_diff < 1, then coming from east.

        # When I come from the north, LHS is east, RHS is west.
        # Order is w/e, n/s
        nbr, nbr_coord, origin = neighbors(matrix, coordinate)

        if matrix[coordinate] == '-':
            if y_diff > 0: # coming from the west, so the north coordinate must be changed.
                try:
                    pipe_matrix_LHS[nbr_coord['north']] = 2
                except:
                    pass
                try:
                    pipe_matrix_RHS[nbr_coord['south']] = 2
                except:
                    pass
            elif y_diff < 0:
                try:
                    pipe_matrix_LHS[nbr_coord['south']] = 2
                except:
                    pass
                try:
                    pipe_matrix_RHS[nbr_coord['north']] = 2
                except:
                    pass
        elif matrix[coordinate] == '|':
            if x_diff > 0: # coming from the north, so the east coordinate must be changed.
                try:
                    pipe_matrix_LHS[nbr_coord['east']] = 2
                except:
                    pass
                try:
                    pipe_matrix_RHS[nbr_coord['west']] = 2
                except:
                    pass
            elif x_diff < 0:
                try:
                    pipe_matrix_LHS[nbr_coord['west']] = 2
                except:
                    pass
                try:
                    pipe_matrix_RHS[nbr_coord['east']] = 2
                except:
                    pass
        elif matrix[coordinate] == 'F':
            if x_diff < 0: # coming from the south. Only LHS, while on RHS there is nothing.
                try: pipe_matrix_LHS[nbr_coord['west']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['north']] = 2
                except: pass
            if y_diff < 0: # coming from the east
                try: pipe_matrix_RHS[nbr_coord['north']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['west']] = 2
                except: pass
        elif matrix[coordinate] == '7':
            if x_diff < 0: # coming from the south.
                try: pipe_matrix_RHS[nbr_coord['east']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['north']] = 2
                except: pass
            if y_diff > 0: # coming from the west
                try: pipe_matrix_LHS[nbr_coord['north']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['east']] = 2
                except: pass
        elif matrix[coordinate] == 'J':
            if x_diff > 0: # coming from the north. Only LHS, while on RHS there is nothing.
                try: pipe_matrix_LHS[nbr_coord['east']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['south']] = 2
                except: pass
            if y_diff > 0: # coming from the west
                try: pipe_matrix_RHS[nbr_coord['south']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['east']] = 2
                except: pass
        elif matrix[coordinate] == 'L':
            if x_diff > 0: # coming from the north. 
                try: pipe_matrix_RHS[nbr_coord['west']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['south']] = 2
                except: pass
            if y_diff < 0: # coming from the east
                try: pipe_matrix_LHS[nbr_coord['south']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['west']] = 2
                except: pass

    previous_coordinate = coordinate

# Swap LHS and RHS
previous_coordinate = start_coordinate

for coordinate in second_direction[1:]:
    if matrix[coordinate] in inside_out.keys(): # It is a '-' or a '|'
        in_out = inside_out[matrix[coordinate]]
        
        # Find the previous coordinate to see where you are coming from
        x_diff = coordinate[0] - previous_coordinate[0]
        y_diff = coordinate[1] - previous_coordinate[1]

        # If x_diff > 0, then coming from north. If x_diff < 1, then coming from south.
        # If y_diff > 0, then coming from west. If y_diff < 1, then coming from east.

        # When I come from the north, LHS is east, RHS is west.
        # Order is w/e, n/s
        nbr, nbr_coord, origin = neighbors(matrix, coordinate)

        if matrix[coordinate] == '-':
            if y_diff > 0: # coming from the west, so the north coordinate must be changed.
                try:
                    pipe_matrix_RHS[nbr_coord['north']] = 2
                except:
                    pass
                try:
                    pipe_matrix_LHS[nbr_coord['south']] = 2
                except:
                    pass
            elif y_diff < 0:
                try:
                    pipe_matrix_RHS[nbr_coord['south']] = 2
                except:
                    pass
                try:
                    pipe_matrix_LHS[nbr_coord['north']] = 2
                except:
                    pass
        elif matrix[coordinate] == '|':
            if x_diff > 0: # coming from the north, so the east coordinate must be changed.
                try:
                    pipe_matrix_RHS[nbr_coord['east']] = 2
                except:
                    pass
                try:
                    pipe_matrix_LHS[nbr_coord['west']] = 2
                except:
                    pass
            elif x_diff < 0:
                try:
                    pipe_matrix_RHS[nbr_coord['west']] = 2
                except:
                    pass
                try:
                    pipe_matrix_LHS[nbr_coord['east']] = 2
                except:
                    pass
        elif matrix[coordinate] == 'F':
            if x_diff < 0: # coming from the south. Only LHS, while on RHS there is nothing.
                try: pipe_matrix_RHS[nbr_coord['west']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['north']] = 2
                except: pass
            if y_diff < 0: # coming from the east
                try: pipe_matrix_LHS[nbr_coord['north']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['west']] = 2
                except: pass
        elif matrix[coordinate] == '7':
            if x_diff < 0: # coming from the south.
                try: pipe_matrix_LHS[nbr_coord['east']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['north']] = 2
                except: pass
            if y_diff > 0: # coming from the west
                try: pipe_matrix_RHS[nbr_coord['north']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['east']] = 2
                except: pass
        elif matrix[coordinate] == 'J':
            if x_diff > 0: # coming from the north. Only LHS, while on RHS there is nothing.
                try: pipe_matrix_RHS[nbr_coord['east']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['south']] = 2
                except: pass
            if y_diff > 0: # coming from the west
                try: pipe_matrix_LHS[nbr_coord['south']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['east']] = 2
                except: pass
        elif matrix[coordinate] == 'L':
            if x_diff > 0: # coming from the north. 
                try: pipe_matrix_LHS[nbr_coord['west']] = 2
                except: pass
                try: pipe_matrix_LHS[nbr_coord['south']] = 2
                except: pass
            if y_diff < 0: # coming from the east
                try: pipe_matrix_RHS[nbr_coord['south']] = 2
                except: pass
                try: pipe_matrix_RHS[nbr_coord['west']] = 2
                except: pass

    previous_coordinate = coordinate

for coordinate in first_direction:
    pipe_matrix_LHS[coordinate] = 1
    pipe_matrix_RHS[coordinate] = 1

for coordinate in second_direction:
    pipe_matrix_LHS[coordinate] = 1
    pipe_matrix_RHS[coordinate] = 1

unique, counts = np.unique(pipe_matrix_LHS, return_counts=True)
instances_LHS = dict(zip(unique, counts))

unique, counts = np.unique(pipe_matrix_RHS, return_counts=True)
instances_RHS = dict(zip(unique, counts))

print("The answer to part 2 is:",instances_LHS[2])
print("Or, the answer to part 2 is:",instances_RHS[2])

# 345 is too low
# 904 is too high
# 705 is too high

## Alternative approach: Shoelace formula taken from Reddit tip
long_list = first_direction + list(reversed(second_direction[:-1]))

# Calculate area inside polygon using Shoelace
import itertools

def pairwise(iterable):
    "s -> (s0, s1), (s1, s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

def determinant(coordinate_pair):
    x1 = coordinate_pair[0][0]
    x2 = coordinate_pair[1][0]
    y1 = coordinate_pair[0][1]
    y2 = coordinate_pair[1][1]

    return ((x1*y2)-(y1*x2))

area_times_two = 0
for coordinate_pair in pairwise(long_list):
    area_times_two += determinant(coordinate_pair)

area = area_times_two/2

# Calculate interior points using Pick's theorem
b = len(long_list)-1
i = abs(area) - (b/2) + 1

print("Those previous two are false. The correct answer using Pick's theorem is:",i)