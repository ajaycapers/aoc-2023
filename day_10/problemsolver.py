# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "10"

practice = "day_"+day+"/practice.txt" 
practice2 = "day_"+day+"/practice2.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np

origin_pipe = {'|': ('north','south'), '-': ('east','west'), 'L': ('north','east'), 'J': ('north','west'), 'F': ('south','east'), '7': ('south', 'west')}
pipes = {'|': ('south','north'), '-': ('west','east'), 'L': ('south','west'), 'J': ('south','east'), 'F': ('north','west'), '7': ('north', 'east')}


def neighbors(matrix: np.ndarray, coordinate: tuple):
    x,y = coordinate

    x_len, y_len = np.array(matrix.shape) - 1
    nbr = dict()
    nbr_coord = dict()

    origin = matrix[x][y]

    if x > x_len or y > y_len:
        return nbr
    if x != 0:
        nbr['north'] = matrix[x-1][y]
        nbr_coord['north'] = (x-1,y)
    if y != 0:
        nbr['west'] = matrix[x][y-1]
        nbr_coord['west'] = (x,y-1)
    if x != x_len:
        nbr['south'] = matrix[x+1][y]
        nbr_coord['south'] = (x+1,y)
    if y != y_len:
        nbr['east'] = matrix[x][y+1]
        nbr_coord['east'] = (x,y+1)
    return nbr, nbr_coord, origin
    
def identify_connected_pipes(matrix,coordinate):
    nbr, nbr_coord, origin = neighbors(matrix,coordinate)
    connections = []

    for key in nbr.keys():
        pipe = nbr[key]

        # Check if it is a pipe
        if pipe in pipes:
            # If it is a pipe, check whether it connects in the direction that was sought
            if key in pipes[pipe]:
                # If it does, check whether the original pipe connected in that direction
                if origin != 'S':
                    if key in origin_pipe[origin]:
                        connections.append(nbr_coord[key])
                elif origin == 'S':
                    connections.append(nbr_coord[key])
    
    return connections

# Open the dataset
with open(dataset) as f:
    input = f.read().split('\n')
    matrix = np.empty((len(input),len(input[0])),dtype="str")

    for i,line in enumerate(input):
        for j,char in enumerate(line):
            matrix[i,j] = char

# Find the starting position's coordinate
s = np.where(matrix == 'S')
start_coordinate = tuple(zip(*s))[0]

# Find the first two coordinates
connected_pipes = identify_connected_pipes(matrix,start_coordinate)

first_direction = [start_coordinate]
second_direction = [start_coordinate]

first_direction.append(connected_pipes[0])
second_direction.append(connected_pipes[1])

print(matrix[25:30,77:81])

while first_direction[-1] != second_direction[-1]:

    # First do the first_direction
    connected_pipes = identify_connected_pipes(matrix,first_direction[-1])
    if connected_pipes[0] not in first_direction:
        first_direction.append(connected_pipes[0])
    elif connected_pipes[1] not in first_direction:
        first_direction.append(connected_pipes[1])
    else:
        raise Exception(''.join(["Some case not foreseen at ",str(connected_pipes),". The list of directions is: ",str(first_direction)]))

    # Then do the second direction
    connected_pipes = identify_connected_pipes(matrix,second_direction[-1])

    if connected_pipes[0] not in second_direction:
        second_direction.append(connected_pipes[0])
    elif connected_pipes[1] not in second_direction:
        second_direction.append(connected_pipes[1])
    else:
        raise Exception(''.join(["Some case not foreseen at ",str(connected_pipes),". The list of directions is: ",str(second_direction)]))

print("The answer to part 1 is: ", len(first_direction)-1)