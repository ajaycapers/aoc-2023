# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "x"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = practice

# Load packages
import numpy as np

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")