# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "3"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = practice

# Load packages
import numpy as np
import scipy.ndimage as ndimage

# Create a function to return if something is a number
def is_a_digit(string):
    try:
        int(string)
        return string
    except:       
        return False

# Check if position is valid    
def isValidPos(i, j, n, m):
 
    if (i < 0 or j < 0 or i > n - 1 or j > m - 1):
        return 0
    return 1 
 
# Function that returns all adjacent elements
def getAdjacent(arr, coord):
    i = coord[0]
    j = coord[1]

    # Size of given 2d array
    n = len(arr)
    m = len(arr[0])
 
    # Initialising a vector array
    # where adjacent element will be stored
    v = []
 
    # Checking for all the possible adjacent positions
    if (isValidPos(i - 1, j - 1, n, m)):
        v.append(arr[i - 1][j - 1])
    if (isValidPos(i - 1, j, n, m)):
        v.append(arr[i - 1][j])
    if (isValidPos(i - 1, j + 1, n, m)):
        v.append(arr[i - 1][j + 1])
    if (isValidPos(i, j - 1, n, m)):
        v.append(arr[i][j - 1])
    if (isValidPos(i, j + 1, n, m)):
        v.append(arr[i][j + 1])
    if (isValidPos(i + 1, j - 1, n, m)):
        v.append(arr[i + 1][j - 1])
    if (isValidPos(i + 1, j, n, m)):
        v.append(arr[i + 1][j])
    if (isValidPos(i + 1, j + 1, n, m)):
        v.append(arr[i + 1][j + 1])

    return v

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")

    input_array = np.array([list(line) for line in input], dtype=object)

i_cell = 0
list_of_numbers = []    # List that stores the numbers that are encountered
number_digits = []      # Variable to temporarily store digits of numbers
symbol_dict = {}

# Loop over the numpy array
with np.nditer(input_array, flags=['c_index','refs_ok'], op_flags=['readwrite']) as it:
    while not it.finished:
        if is_a_digit(it[0]):
            number_digits.append(it[0].tolist())
            it[0] = len(list_of_numbers)+1
        else:
            # If it is a symbol, store it to the dictionary
            if it[0] != '.':
                symbol_dict[it[0].tolist()] = it[0].tolist()

            if number_digits: # List contains something
                list_of_numbers.append(int(''.join(number_digits)))
                number_digits.clear()

        is_next = it.iternext()

print(input_array)

# Find all the neighbors of the numbers and check if they are a symbol
sum_of_numbers = []
for i,number in enumerate(list_of_numbers):
    coordinates = np.transpose((input_array==i+1).nonzero())
    adjacent = set()
    for coordinate in coordinates:
        neighbors = getAdjacent(input_array,coordinate)
        adjacent.update(set(neighbors).intersection(set(symbol_dict)))
    
    if len(adjacent) != 0:
        sum_of_numbers.append(number)
    
print("The answer to part A is: ", sum(sum_of_numbers))

## Part B
potential_gears = np.transpose((input_array=='*').nonzero())
print(potential_gears)
gear_to_be_replaced = []

for potential_gear in potential_gears:
    neighbors = getAdjacent(input_array,potential_gear)
    
    neighboring_parts = list(set([x for x in neighbors if type(x)==int]))

    gear_ratio = []
    if len(neighboring_parts)==2:
        gear_ratio = list_of_numbers[neighboring_parts[0]-1] * list_of_numbers[neighboring_parts[1]-1]
        gear_to_be_replaced.append(gear_ratio)

print("The answer to part B is: ", sum(gear_to_be_replaced))