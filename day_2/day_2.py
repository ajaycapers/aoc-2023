# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "2"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np

# Create a class to store games in
class Game:
    def __init__(self,ID,sets):
        self.ID = ID
        self.sets = sets

    def check_with_replacement(self, sample):
        for set in self.sets:
            for set_value,sample_value in zip(set,sample):
                if sample_value < set_value:
                    return False
        return True

    def fewest_cubes(self):
        lowest_possible = [0,0,0]
        for set in self.sets:
            lowest_possible = np.maximum(set,lowest_possible)
        
        return lowest_possible
        
# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")

# Dictionary for matching the positions of colors
dict_RGB_position = {'red': 0, 'green': 1, 'blue': 2}
my_games = []

# Retrieve the input
for line in input:
    # First retrieve the game number
    splitted_on_colon = line.split(':')
    ID_raw = splitted_on_colon[0].split()
    ID = int(ID_raw[1])

    # Then get the different games
    revelations = splitted_on_colon[1].split(';')
    sets = []
    for revelation in revelations:
        cubes = revelation.split(',')
        RGB = [0] * 3
        for cube in cubes:
            [amount,color] = cube.split()
            RGB[dict_RGB_position[color]] = int(amount)

        sets.append(RGB)
    
    # Store all games in my_games
    my_games.append(Game(ID,sets))

# Assignment A
elf_sample = [12,13,14]

possible_games = []
sum_possible_IDs = 0
for game in my_games:
    ID_possible = [game.ID,game.check_with_replacement(elf_sample)]
    possible_games.append(ID_possible)
    if ID_possible[1]:
        sum_possible_IDs += ID_possible[0]

print("The answer to part A is: ",sum_possible_IDs)

## Part B
power = []
for game in my_games:
    fewest_cubes = game.fewest_cubes()
    outcome = np.prod(fewest_cubes)
    power.append(outcome)

print("The answer to part B is: ",np.sum(power))