# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "11"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np
from itertools import combinations
from scipy.spatial.distance import cdist

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")

    matrix = np.empty((len(input),len(input[0])),dtype="str")

    for i,line in enumerate(input):
        for j,char in enumerate(line):
            matrix[i,j] = char

original_matrix = matrix.copy()

# Expand the galaxies
line_number = 0
while line_number < len(matrix):
    # Count the number of instances of '#' in the line
    unique_values, counts = np.unique(matrix[line_number,:], return_counts=True)
    totals = dict(zip(unique_values, counts))

    # If '#' is not in the line, space needs to expand
    if '#' not in totals:
        matrix = np.insert(matrix,line_number,np.full_like(matrix[line_number],'.'),axis=0)
        line_number += 2    # Increase the line number by 2 because of inserting the extra line
    else:
        line_number += 1

# Repeat the trick, but now on the transposed matrix
column_number = 0
while column_number < len(matrix.T):
    # Count the number of instances of '#' in the line
    unique_values, counts = np.unique(matrix.T[column_number,:], return_counts=True)
    totals = dict(zip(unique_values, counts))

    # If '#' is not in the line, space needs to expand
    if '#' not in totals:
        matrix = np.insert(matrix.T,column_number,np.full_like(matrix.T[column_number],'.'),axis=0).T
        # matrix = matrix_transpose.T
        column_number += 2    # Increase the line number by 2 because of inserting the extra line
    else:
        column_number += 1
    
# Number all the galaxies
galaxy_image = np.zeros_like(matrix, dtype=int)
galaxy_counter = 1
galaxy_coordinates = []
for iy, ix in np.ndindex(matrix.shape): # Just learnt that numpy follows zyx instead of xyz convention
    if matrix[iy,ix] == '#':
        galaxy_image[iy,ix] = galaxy_counter
        galaxy_counter += 1
        galaxy_coordinates.append((iy,ix))

# Iterate over the combinations and count their Manhattan distances using cdist
shortest_paths = []
for galaxy_pair in combinations(galaxy_coordinates,2):
    shortest_path = cdist([galaxy_pair[0]],[galaxy_pair[1]],metric='cityblock')
    shortest_paths.append(shortest_path)

print("The answer to part 1 is:",sum(shortest_paths))

## Part 2

# Find the lines and columns on which to expand the galaxies
line_number = 0
expand_on_lines = []
while line_number < len(original_matrix):
    # Count the number of instances of '#' in the line
    unique_values, counts = np.unique(original_matrix[line_number,:], return_counts=True)
    totals = dict(zip(unique_values, counts))

    # If '#' is not in the line, space needs to expand
    if '#' not in totals:
        expand_on_lines.append(line_number)
    
    line_number += 1

# Repeat the trick, but now on the transposed matrix
column_number = 0
expand_on_columns = []
while column_number < len(original_matrix.T):
    # Count the number of instances of '#' in the line
    unique_values, counts = np.unique(original_matrix.T[column_number,:], return_counts=True)
    totals = dict(zip(unique_values, counts))

    # If '#' is not in the line, space needs to expand
    if '#' not in totals:
         expand_on_columns.append(column_number)
    
    column_number += 1

# Number all the galaxies and find their coordinates
galaxy_coordinates = []
for iy, ix in np.ndindex(original_matrix.shape): # Just learnt that numpy follows zyx instead of xyz convention
    if original_matrix[iy,ix] == '#':
        galaxy_coordinates.append((iy,ix))

# Function to retrieve expanded coordinate
def expanded_coordinate(coordinate: tuple, expand_on_lines: list, expand_on_columns: list, expansion_coefficient: int):
    y = coordinate[0]
    x = coordinate[1]

    y_expansion = sum(i < y for i in expand_on_lines)
    x_expansion = sum(i < x for i in expand_on_columns)

    new_coordinate = (y + y_expansion * expansion_coefficient, x + x_expansion * expansion_coefficient)
    return new_coordinate

# Iterate over the combinations and count their Manhattan distances using cdist
expansion_amount = 1000000-1

shortest_paths = []
for galaxy_pair in combinations(galaxy_coordinates,2):
    expanded_coordinate_one = expanded_coordinate(galaxy_pair[0],expand_on_lines,expand_on_columns,expansion_amount)
    expanded_coordinate_two = expanded_coordinate(galaxy_pair[1],expand_on_lines,expand_on_columns,expansion_amount)
    

    shortest_path = cdist([expanded_coordinate_one],[expanded_coordinate_two],metric='cityblock')
    shortest_paths.append(shortest_path)

answer_part_2 = sum(shortest_paths)
print ("The answer to part 2 is:",answer_part_2)
print ("The answer to part 2 is:",np.sum(answer_part_2))
# 7.46962098e+11 is not the right answer
# 746962098000 is too high
# 746962097860 should be correct?!?!?!