# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "8"

practice = "day_"+day+"/practice.txt" 
practice2 = "day_"+day+"/practice_2.txt" 
practice3 = "day_"+day+"/practice_3.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

class Node():
    def __init__(self,id,elements):
        self.id = id
        self.pointers = elements

    def turn(self,direction):
        if direction == 'L':
            return self.pointers[0]
        elif direction == 'R':
            return self.pointers[1]
        
class LinkedList():
    def __init__(self):
        self.head = None
    
# Load packages
import numpy as np
from math import lcm

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")
    directions = input.pop(0)
    input.remove('')

    nodes = dict()
    for line in input:
        node_ID = line.split('=')[0].strip()
        elements = tuple(line.split('=')[1].strip().replace('(','').replace(')','').replace(' ','').split(','))
        nodes[node_ID] = Node(node_ID,elements) 

# start_node = 'AAA'
# final_node = 'ZZZ'

# node = start_node
# direction_position = 0
# direction_length = len(directions)

# count_steps = 0
# while node != final_node:
#     if (direction_position // direction_length) == 1:
#         direction_position = 0
    
#     direction = directions[direction_position]
#     node = nodes[node].turn(direction)

#     count_steps += 1
#     direction_position += 1

# print("The answer to part A is: ",count_steps)

### Part B ###
start_nodes = set([id for id in nodes.keys() if id[-1] == 'A'])
final_nodes = set([id for id in nodes.keys() if id[-1] == 'Z'])

node_set = start_nodes
direction_position = 0
direction_length = len(directions)

count_steps = 0
# while not node_set.issubset(final_nodes):
#     if (direction_position // direction_length) == 1:
#         direction_position = 0
    
#     direction = directions[direction_position]
#     node_set = set([nodes[node].turn(direction) for node in node_set])

#     count_steps += 1
#     direction_position += 1

LCM_array = []

for node in start_nodes:
    direction_position = 0
    direction_length = len(directions)

    count_steps = 0
    while node not in final_nodes:
        if (direction_position // direction_length) == 1:
            direction_position = 0
        
        direction = directions[direction_position]
        node = nodes[node].turn(direction)

        count_steps += 1
        direction_position += 1

    LCM_array.append(count_steps)

print(LCM_array)

lcm(*LCM_array)
    
print("The answer to part B is: ",lcm(*LCM_array))

