day = "1"

practice = "day_"+day+"/practice.txt" 
practice2 = "day_"+day+"/practice2.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np

# Open the dataset
with open(dataset) as f:
    input = f.read()
    row_list = input.split("\n")

## Part A
calibration_value = []
for row in row_list:
    first_digit, last_digit = [], []

    # Find first digit
    for character in row:
        if character.isdigit():
            first_digit = character
            break

    # Find last digit
    for character in reversed(row):
        if character.isdigit():
            last_digit = character
            break

    number = ''.join([first_digit, last_digit])
    calibration_value.append(int(number))

total_calibration_value = sum(calibration_value)
print("The answer to part A is: ",total_calibration_value)

## Part B
import re
from word2number import w2n

# Create a list with items that need to be matched
match_list = ['one','two','three','four','five','six','seven','eight','nine','0','1','2','3','4','5','6','7','8','9']

# Create a pattern for the re-function. They are joined by '|' to look for all values. The preceding ?= is required to match overlapping characters.
pattern = r'(?=({}))'.format('|'.join(map(re.escape, match_list)))

# Initiate an empty array for the values
calibration_values = []

# Loop over all rows
for row in row_list:
    # Find all matches of the patterns in the match_list in the row
    values = re.findall(pattern, row)
    # If the match is a string (spelled-out word), convert it to a digit, else, take the digit. In all cases, convert them to strings in order to be able to join them later.
    values = [str(w2n.word_to_num(item)) if isinstance(item,str) else str(item) for item in values]

    # Combine the two digits (stored as strings) and make an integer out of it.
    calibration_values.append(int(''.join([values[0],values[-1]])))

# Finally sum the numbers
print("The answer to part B is: ",sum(calibration_values))
