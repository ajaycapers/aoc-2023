# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "7"

practice = "day_"+day+"/practice.txt" 
input = "day_"+day+"/input.txt" 

dataset = input

# Load packages
import numpy as np
from queue import PriorityQueue

# Define the game information
card_order = ('A','K','Q','J','T','9','8','7','6','5','4','3','2','1')
card_values = {x: i for i,x in enumerate(card_order)}
hands_sorted_on_strength = PriorityQueue()

joker_card_order = ('A','K','Q','T','9','8','7','6','5','4','3','2','J')
joker_card_values = {x: i for i,x in enumerate(card_order)}

# find_type returns the priority for the type
# It will return 0 for 5 of a kind, 1 for four of a kind, 2 for full-house, 3 for 3-of-a-kind, 4 for 2-pair, 5 for 1-pair and 6 for highest
def find_type_priority(hand): # Hand should be provided as string
    if type(hand) == str:
        hand = [*hand]
    
    cards = set(hand)

    if len(cards) == 1:
        return 0 # 5-of-a-kind
    elif len(cards) == 5:
        return 6 # highste value
    elif len(cards) == 4:
        return 5 # one pair
    else:
        highest_x = []
        for card in cards:
            x = hand.count(card)
            highest_x.append(x)
        if max(highest_x) == 4:
            return 1 # 4-of-a-kind
        elif max(highest_x) == 2:
            return 4 # two pairs
        else:
            if len(cards) == 2:
                return 2 # full house
            else:
                return 3 # 3-of-a-kind

# find_type returns the priority for the type
# It will return 0 for 5 of a kind, 1 for four of a kind, 2 for full-house, 3 for 3-of-a-kind, 4 for 2-pair, 5 for 1-pair and 6 for highest
def find_joker_type_priority(hand): # Hand should be provided as string
    if type(hand) == str:
        hand = [*hand]
    
    cards = set(hand)
    if 'J' not in cards:
        return find_type_priority(hand)
    
    no_of_jokers = hand.count('J')

    if len(cards) == 2 or len(cards) == 1: #JxAn or JJJJJ
        return 0 # 5-of-a-kind, namely jokers and one other card
    elif len(cards) == 5: # JAKQT
        return 5 # one pair
    elif len(cards) == 3:
        if no_of_jokers == 3: # JJJAK
            return 1 # 4-of-a-kind
        elif no_of_jokers == 2: #JJAAK
            return 1 # 4-of-a-kind
        elif no_of_jokers == 1: #JAAKK
            return 2 # full house
    elif len(cards) == 4:
        if no_of_jokers == 2: # JJAKQ
            return 3 # 3-of-a-kind
        elif no_of_jokers == 1: #JAAKQ
            return 3 # 3-of-a-kind
    else:
        print("There must be a scenario you have overseen. The hand is:",hand)
    
def find_card_priority(hand): # Hand should be provided as string
    if type(hand) == str:
        hand = [*hand]

    card_priorities = [card_values[card] for card in hand]
    return card_priorities

def find_joker_card_priority(hand): # Hand should be provided as string
    if type(hand) == str:
        hand = [*hand]

    joker_card_priorities = [joker_card_values[card] for card in hand]
    return joker_card_priorities

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")
    hands_and_bids = []
    for line in input:
        hand,bid = line.split()
        hands_and_bids.append((hand,int(bid)))

for hand_and_bid in hands_and_bids:
    type_priority = find_type_priority(hand_and_bid[0])
    card_priorities = find_card_priority(hand_and_bid[0])
    # We need to multiply by minus one as we need to get the weakest cards first
    input_for_priority_cue = tuple([-type_priority,*[-x for x in card_priorities],hand_and_bid[1]])

    hands_sorted_on_strength.put(input_for_priority_cue)

# Empty the priority queue and count the values
multiplier = 1
total_winnings = 0
while not hands_sorted_on_strength.empty():
    current_card = hands_sorted_on_strength.get()
    total_winnings += multiplier * current_card[6]
    multiplier += 1

print("The answer to part A is: ",total_winnings)

### Part B ###
for hand_and_bid in hands_and_bids:
    joker_type_priority = find_joker_type_priority(hand_and_bid[0])
    joker_card_priorities = find_joker_card_priority(hand_and_bid[0])
    # We need to multiply by minus one as we need to get the weakest cards first
    input_for_priority_cue = tuple([-joker_type_priority,*[-x for x in joker_card_priorities],hand_and_bid[1]])

    hands_sorted_on_strength.put(input_for_priority_cue)

# Empty the priority queue and count the values
multiplier = 1
total_winnings = 0
while not hands_sorted_on_strength.empty():
    current_card = hands_sorted_on_strength.get()
    total_winnings += multiplier * current_card[6]
    multiplier += 1

print("The answer to part B is: ",total_winnings)

# 253506878 is too high
# 253254392 is too high
# 253253225 should be correct (Marc's script)
# Can't find the correct answer, but I'm very close, so used Marc's script to progress. Would be good to revisit this later.