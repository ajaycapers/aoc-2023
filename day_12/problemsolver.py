# Initial comments to make loading the dataset easier. Change the day and change the type of the dataset.
day = "12"

practice = "day_"+day+"/practice.txt" 
practice2 = "day_"+day+"/practice2.txt" 
practice_undamaged = "day_"+day+"/practice_undamaged.txt" 
input = "day_"+day+"/input.txt" 

dataset = practice2

# Load packages
import numpy as np
from itertools import permutations
import cProfile
import re

class Condition_Record():
    def __init__(self,state: str,summary: list):
        self.damaged_state = state
        self.state_summary = summary

        # Create the possible states as sets to directly remove double entries
        self.all_possible_states = set() # Will contain wrong states as well
        self.possible_correct_states = set() # Will only contain correct states

    def create_permutations(self):
        # Finding all permutations is too slow. Therefore, we first identify the chunks of ?
        ind_question_mark = [i for i, ltr in enumerate(self.damaged_state) if ltr == '?']
        print(ind_question_mark)        
        
        unique_values, counts = np.unique(list(self.damaged_state), return_counts=True)
        totals = dict(zip(unique_values, counts))
        n_question_marks = totals['?']
        try: n_hashes = totals['#']
        except: n_hashes = 0
        n_total_hashes = sum(self.state_summary)
        n_hashes_missing = n_total_hashes - n_hashes
        permutation_input = ['#']*n_hashes_missing + ['.']*(n_question_marks - n_hashes_missing)
        options = set(permutations(permutation_input))
        print("The number of options is:",len(options))

        for option in options:
            counter = 0
            new_state = [*self.damaged_state]
            for i,element in enumerate([*self.damaged_state]):
                if element == '?':
                    new_state[i] = option[counter]
                    counter += 1

            self.all_possible_states.add(''.join(new_state))

    def generate_summary(self,state):
        n_damaged_springs = 0
        summary = []
        for char in state:
            if char == '#':
                n_damaged_springs += 1
            elif char == '.':
                summary.append(n_damaged_springs)
                n_damaged_springs = 0
            else:
                raise Exception("This character is not allowed here.")

        summary.append(n_damaged_springs)
        summary[:] = [i for i in summary if i != 0]
        return summary
    
    def verify_state(self,state,summary):
        if self.generate_summary(state) == summary:
            return True
        else:
            return False

    def verify_permutations(self):
        self.possible_correct_states.update([state for state in self.all_possible_states if self.verify_state(state,self.state_summary)])
        
        return len(self.possible_correct_states)

# Open the dataset
with open(dataset) as f:
    input = f.read().split("\n")

records = []
for line in input:
    state = line.split()[0]
    summary = [int(x) for x in line.split()[1].split(',')]

    records.append(Condition_Record(state,summary))

total_arrangements = 0
for i,record in enumerate(records):
    cProfile.run("record.create_permutations()")
    number_of_arrangements = record.verify_permutations()
    
    total_arrangements += number_of_arrangements

    print(i,"/1000 processed")

print("The answer to part 1 is:", total_arrangements)